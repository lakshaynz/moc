const root = document.getElementById("root")
let God_view_list = [
    ["uncaring", " regret"],
    ["indifferent", " pride"],
    ["denying", " desire"],
    ["punitive", " fear"],
    ["Evil", " anger / blame / guilt"],
    ["wants me dead, wants to kill me ", "shame"],
    ["Loving", "love"],
    ["One", "Joy"],
    ["wise", "reason"],
    ["permitting", "Courage"],
    ["enabling", "Satisfactory"],
    ["forgiving", "Acceptance"],
]

let people_view_list = [
    ["Dumb/stupid",  "pride"],
    ["kind",  "loving"],
    ["Evil",  "guilt / regret / anger"],
    ["Theifs",  "fear"],
    ["better / luckier than me ",  "desire / envy"],
    ["bad choosers",  "anger / envy / desire"],
    ["hate me",  "shame"],
    ["uncaring / indifferent",  "anger / guilt / pride"],
    ["mad",  "fear"],
    ["unforgiving",  "guilt"],
    ["rejecting",  "shame"],

]

let life_view_list = [
    ["full of problems",  "Apathy / regret"],
    ["painful",  "shame / guilt"],
    ["very difficult" ,  "desire"],
    ["Dangerous",  "fear"],
    ["frustrating",  "anger"],
    ["not fun, not pleasure",  "desire"],
    ["full of solutions" , "reason "],
]

let self_view_list = [
    ["poor",  "desire"],
    ["better than others",  "pride"],
    ["small",  "fear"],
    ["Bad",  "guilt"],
    ["wanting to kill myself",  "shame"],
    ["no energy",  "apathy"],
    ["victim",  "guilt blame "],
    ["right",  "pride"],
    ["loving", "love"],

]

let god_view_div = document.createElement('div')
let life_view_div = document.createElement('div')
let people_view_div = document.createElement('div')
function generate(the_div,the_list){
    the_div.style.border = "solid black 2px"
    the_list.forEach((item,index)=>{
    let box = document.createElement('div')
    box.innerText= item[0]
    box.style.border = "solid black 2px"
    box.style.padding = "10px"
    box.style.margin = "10px"
    box.style.display = "inline-block"
    box.style.width = "100px"
    box.style.height = "100px"
    box.onclick = () => {
        box.innerText = item[1]
        setTimeout(()=>box.innerText = item[0], 2000)
    }
    the_div.appendChild(box)
    })
    root.appendChild(the_div)
}

let bla1 = document.createElement("div")
root.appendChild(bla1)
bla1.innerText="god view"
generate(god_view_div, God_view_list, )

let bla2 = document.createElement("div")
root.appendChild(bla2)
bla2.innerText="ppl view"

generate(people_view_div,people_view_list )

let bla3 = document.createElement("div")
root.appendChild(bla3)
bla3.innerText="life view"

generate(life_view_div,life_view_list )

